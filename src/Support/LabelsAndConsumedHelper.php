<?php

declare(strict_types=1);

namespace Smorken\Dns\Support;

use Smorken\Dns\ValueObjects\DomainVO;

class LabelsAndConsumedHelper
{
    public array $labels = [];

    public function __construct(public int $consumed) {}

    public function addLabel(string $label): void
    {
        $this->labels[] = $label;
    }

    public function emptyCreator(): self
    {
        return new self(0);
    }

    public function incrementConsumed(int $consumed): void
    {
        $this->consumed += $consumed;
    }

    public function mergeLabels(array $labels): void
    {
        $this->labels = array_merge($this->labels, $labels);
    }

    public function toDomainVO(): DomainVO
    {
        return new DomainVO($this->convertLabelsArrayToDomain(), $this->consumed ?: null);
    }

    private function convertLabelsArrayToDomain(): ?string
    {
        if (! $this->labels) {
            return null;
        }

        return implode(
            '.',
            array_map(
                fn ($label) => \addcslashes((string) $label, "\0..\40.\177"),
                $this->labels
            )
        );
    }
}
