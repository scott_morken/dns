<?php

namespace Smorken\Dns\Labels\Answer;

class IpAddress extends Standard
{
    public function from(string $name): string
    {
        return inet_ntop($name);
    }
}
