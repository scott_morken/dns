<?php

namespace Smorken\Dns\Labels\Answer;

use Smorken\Dns\Parsers\Traits\Labels;

class Hostname extends Standard
{
    use Labels;

    public function from(string $name): string
    {
        $vo = $this->readDomain($name, 0);

        return $vo->domain;
    }
}
