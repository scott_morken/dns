<?php

namespace Smorken\Dns\Labels\Answer;

use Smorken\Dns\Contracts\Labels\AnswerLabel;

class Standard implements AnswerLabel
{
    public function from(string $name): string
    {
        return $name;
    }
}
