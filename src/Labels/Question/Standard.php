<?php

namespace Smorken\Dns\Labels\Question;

use Smorken\Dns\Contracts\Labels\QuestionLabel;

class Standard implements QuestionLabel
{
    public function from(string $name): string
    {
        return $name;
    }

    public function to(string $name): string
    {
        return $name;
    }
}
