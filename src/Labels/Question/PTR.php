<?php

namespace Smorken\Dns\Labels\Question;

use Smorken\Dns\Exception;

class PTR extends Standard
{
    public function from(string $name): string
    {
        $labels = explode('.', $name);
        $labels = array_reverse(array_slice($labels, 0, (count($labels) - 2)));
        if (stripos($name, 'ip6') === false) {
            return implode('.', $labels);
        }
        $parts = [];
        while (count($labels)) {
            $parts[] = implode('', array_slice($labels, 0, 4));
            $labels = array_slice($labels, 4);
        }

        return implode(':', $parts);
    }

    public function to(string $name): string
    {
        if (@inet_pton($name) === false) {
            throw new Exception('Invalid IP address.');
        }
        if (! str_contains($name, ':')) {
            return inet_ntop(strrev(inet_pton($name))).'.in-addr.arpa';
        }

        return wordwrap(strrev(bin2hex(inet_pton($name))), 1, '.', true).'.ip6.arpa';
    }
}
