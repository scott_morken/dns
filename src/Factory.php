<?php

namespace Smorken\Dns;

use Smorken\Dns\Contracts\Models\Message;
use Smorken\Dns\Transports\Udp;

class Factory
{
    public static ?\Smorken\Dns\Contracts\Query $query = null;

    public static function makeQuery(): Contracts\Query
    {
        if (! self::$query) {
            $config = require_once __DIR__.'/../config/config.php';
            $servers = $config['servers'] ?? ['8.8.8.8'];
            $timeout = $config['timeout'] ?? 1.0;
            $transports = [];
            foreach ($servers as $server) {
                $transports[] = new Udp($server, $timeout);
            }
            $handler = new Handler(new Pointer);
            self::$query = new Query($handler, $transports);
        }

        return self::$query;
    }

    public static function query(string $name, int $type = Message::TYPE_A, int $class = Message::CLASS_IN): ?Message
    {
        $query = self::makeQuery();

        return $query->query($name, $type, $class);
    }
}
