<?php

namespace Smorken\Dns\Contracts\Transports;

interface Transport
{
    public function send(string $packet): string;

    public function setNameserver(string $nameserver): Transport;

    public function setTimeout(float $timeout): Transport;
}
