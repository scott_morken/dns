<?php

namespace Smorken\Dns\Contracts\Labels;

interface Label
{
    public function from(string $name): string;
}
