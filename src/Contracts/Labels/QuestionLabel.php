<?php

namespace Smorken\Dns\Contracts\Labels;

interface QuestionLabel extends Label
{
    public function to(string $name): string;
}
