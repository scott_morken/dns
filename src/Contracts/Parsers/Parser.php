<?php

namespace Smorken\Dns\Contracts\Parsers;

use Smorken\Dns\Contracts\Models\Model;

/**
 * @template TModel of Model
 */
interface Parser
{
    /**
     * @phpstan-param TModel $model
     *
     * @phpstan-return TModel|null
     */
    public function fromBinary(Model $model): ?Model;

    /**
     * @phpstan-param TModel $model
     */
    public function toBinary(Model $model): string;
}
