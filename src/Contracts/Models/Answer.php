<?php

namespace Smorken\Dns\Contracts\Models;

/**
 * Interface Answer
 *
 *
 * @property int $ttl time to live
 * @property string $name answer name
 * @property int $type Message::TYPE_?
 * @property int $class Message::CLASS_IN
 * @property string|string[]|array|null $data payload data for answer
 * @property string $raw_data raw payload data
 * @property string $rdata raw response data
 *
 * @phpstan-require-extends \Smorken\Dns\Models\Answer
 */
interface Answer extends Model
{
    public function newAnswer(
        string $name,
        int $type,
        int $ttl,
        string $data,
        int $class = Message::CLASS_IN
    ): Answer;
}
