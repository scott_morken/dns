<?php

namespace Smorken\Dns\Contracts\Models;

/**
 * Interface Message
 *
 *
 * @property Header $header
 * @property array<Question> $questions
 * @property array<Answer> $answers
 *
 * @phpstan-require-extends \Smorken\Dns\Models\Message
 */
interface Message extends Model
{
    public const CLASS_IN = 1; // standard internet type query

    public const OPCODE_IQUERY = 1; // inverse query, shouldn't be used anymore, use TYPE_PTR instead

    public const OPCODE_QUERY = 0; //use for standard questions

    public const OPCODE_STATUS = 2;

    public const RCODE_FORMAT_ERROR = 1;

    public const RCODE_NAME_ERROR = 3;

    public const RCODE_NOT_IMPLEMENTED = 4;

    public const RCODE_OK = 0;

    public const RCODE_REFUSED = 5;

    public const RCODE_SERVER_FAILURE = 2;

    public const TYPE_A = 1;

    public const TYPE_AAAA = 28;

    public const TYPE_ANY = 255;

    public const TYPE_CAA = 257;

    public const TYPE_CNAME = 5;

    public const TYPE_MX = 15;

    public const TYPE_NS = 2;

    public const TYPE_OPT = 41;

    public const TYPE_PTR = 12;

    public const TYPE_SOA = 6;

    public const TYPE_TXT = 16;

    public function firstAnswer(): string|array|null;
}
