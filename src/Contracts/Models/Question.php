<?php

namespace Smorken\Dns\Contracts\Models;

/**
 * Interface Question
 *
 *
 * @property string $name query name, ip, hostname
 * @property int $type query type (QTYPE), Message::TYPE_
 * @property int $class query class (QCLASS), Message::CLASS_IN (internet)
 * @property string $rdata raw data from response
 *
 * @phpstan-require-extends \Smorken\Dns\Models\Question
 */
interface Question extends Model
{
    public function newQuestion(
        string $name,
        int $type = Message::TYPE_A,
        int $class = Message::CLASS_IN
    ): Question;
}
