<?php

namespace Smorken\Dns\Contracts\Models;

/**
 * Interface Header
 *
 *
 * @property int $id 16 bit message ID
 * @property bool $qr Query/Response flag, query = false, response = true
 * @property int $opcode kind of query, 4 bit
 * @property bool $aa Authoritative Answer
 * @property bool $tc TrunCation
 * @property bool $rd Recursion Desired
 * @property bool $ra Recursion Available
 * @property int $rcode Response Code, 4 bit
 * @property int $qdcount question count, 16 bit
 * @property int $ancount answer count, 16 bit
 * @property int $nscount name server count, 16 bit
 * @property int $arcount authority record (resources) count, 16 bit
 * @property string $rdata raw binary data from response
 *
 * @phpstan-require-extends \Smorken\Dns\Models\Header
 */
interface Header extends Model
{
    public function forQuestion(array $attrs = []): Header;

    /**
     * 16 bit int random message ID
     */
    public function generateId(): self;
}
