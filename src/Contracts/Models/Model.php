<?php

namespace Smorken\Dns\Contracts\Models;

/**
 * @phpstan-require-extends \Smorken\Dns\Models\Base
 */
interface Model
{
    public function __construct(array $attrs = []);

    public function getAttribute(string $attr): mixed;

    public function newInstance(array $attrs = []): static;

    public function setAttribute(string $attr, $value): static;

    public function setAttributes(array $attrs = []): static;

    public function toArray(): array;
}
