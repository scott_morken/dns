<?php

namespace Smorken\Dns\Contracts;

use Smorken\Dns\Contracts\Labels\Label;
use Smorken\Dns\Contracts\Models\Message;
use Smorken\Dns\Contracts\Models\Model;
use Smorken\Dns\Contracts\Parsers\Parser;

interface Handler
{
    public function fromBinary(string $binary): Message;

    public function getLabel(int $label_id, int $which): Label;

    public function getModel(string $model_contract): Model;

    public function getParser(int $parser_id): Parser;

    public function newQuery(string $name, int $type = Message::TYPE_A, int $class = Message::CLASS_IN): Message;

    public function toBinary(Message $message): string;
}
