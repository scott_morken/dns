<?php

namespace Smorken\Dns\Contracts;

use Smorken\Dns\Contracts\Models\Message;
use Smorken\Dns\Contracts\Transports\Transport;

interface Query
{
    public function addTransport(Transport $transport): Query;

    public function getHandler(): Handler;

    public function query(string $name, int $type = Message::TYPE_A, int $class = Message::CLASS_IN): ?Message;

    public function setHandler(Handler $handler): Query;

    /**
     * @param  Transport[]  $transports
     */
    public function setTransports(array $transports): Query;
}
