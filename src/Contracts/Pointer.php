<?php

namespace Smorken\Dns\Contracts;

interface Pointer
{
    public const ANSWER = 1;

    public const QUESTION = 0;

    public function getCurrent(): int;

    public function getStart(): int;

    public function incrementCurrent(int $increment = 1): Pointer;

    /**
     * @param  int-mask-of<\Smorken\Dns\Contracts\Pointer::*>  $which
     */
    public function next(
        string $binary,
        int $which = self::QUESTION
    ): Pointer;

    public function reset(int $index): Pointer;

    public function setCurrent(int $index): Pointer;
}
