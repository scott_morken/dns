<?php

namespace Smorken\Dns;

use Smorken\Dns\Contracts\Handler;
use Smorken\Dns\Contracts\Models\Message;
use Smorken\Dns\Contracts\Transports\Transport;

class Query implements \Smorken\Dns\Contracts\Query
{
    protected Handler $handler;

    /**
     * @var Transport[]
     */
    protected array $transports = [];

    public function __construct(Handler $handler, array $transports)
    {
        $this->setHandler($handler);
        $this->setTransports($transports);
    }

    /**
     * {@inheritDoc}
     */
    public function addTransport(Transport $transport): Contracts\Query
    {
        $this->transports[] = $transport;

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getHandler(): Handler
    {
        return $this->handler;
    }

    /**
     * {@inheritDoc}
     */
    public function setHandler(Handler $handler): Contracts\Query
    {
        $this->handler = $handler;

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function query(string $name, int $type = Message::TYPE_A, int $class = Message::CLASS_IN): Message
    {
        $message = $this->getHandler()->newQuery($name, $type, $class);
        $packet = $this->getHandler()->toBinary($message);
        $response = null;
        foreach ($this->transports as $transport) {
            $response = $this->tryTransport($transport, $packet);
            if ($response) {
                break;
            }
        }
        if ($response) {
            return $this->getHandler()->fromBinary($response);
        }

        // return question message by default
        return $message;
    }

    /**
     * {@inheritDoc}
     */
    public function setTransports(array $transports): Contracts\Query
    {
        $this->transports = $transports;

        return $this;
    }

    protected function tryTransport(Transport $transport, string $packet): string
    {
        return $transport->send($packet);
    }
}
