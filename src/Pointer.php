<?php

namespace Smorken\Dns;

class Pointer implements \Smorken\Dns\Contracts\Pointer
{
    protected int $current = 0;

    protected int $start = 0;

    public function __construct(int $start = 0)
    {
        $this->reset($start);
    }

    public function getCurrent(): int
    {
        return $this->current;
    }

    public function setCurrent(int $index): Contracts\Pointer
    {
        $this->current = $index;

        return $this;
    }

    public function getStart(): int
    {
        return $this->start;
    }

    public function incrementCurrent(int $increment = 1): Contracts\Pointer
    {
        $this->current += $increment;

        return $this;
    }

    /**
     * @param  int-mask-of<self::*>  $which
     */
    public function next(
        string $binary,
        int $which = self::QUESTION
    ): Contracts\Pointer {
        $this->reset($this->current);
        match ($which) {
            self::ANSWER => $this->nextAnswer($binary),
            self::QUESTION => $this->nextQuestion($binary),
        };

        return $this;
    }

    public function reset(int $index): Contracts\Pointer
    {
        $this->start = $index;
        $this->setCurrent($index);

        return $this;
    }

    protected function nextAnswer(string $binary): void
    {
        $consumed = $this->start;
        $consumed = $this->walkAnswer($binary, $consumed);
        if (is_null($consumed)) {
            return;
        }
        $this->setCurrent($consumed);
    }

    protected function nextQuestion(string $binary): void
    {
        $consumed = $this->start;
        $consumed = $this->walkQuestion($binary, $consumed);
        if (is_null($consumed) || ! isset($binary[$consumed + 4 - 1])) {
            return;
        }
        $consumed += 4;
        $this->setCurrent($consumed);
    }

    protected function walkAnswer(string $data, int $consumed): ?int
    {
        if (! isset($data[$consumed + 10 - 1])) {
            return null;
        }
        $consumed += 10;
        [$rdLength] = array_values(unpack('n', substr($data, $consumed, 2)));
        $consumed += 2;
        $consumed += $rdLength;

        if (! isset($data[$consumed - 1])) {
            return null;
        }

        return $consumed;
    }

    protected function walkQuestion(string $data, int $consumed): ?int
    {
        while (true) {
            if (! isset($data[$consumed])) {
                return null;
            }

            $length = \ord($data[$consumed]);

            // end of labels reached
            if ($length === 0) {
                $consumed += 1;
                break;
            }

            // first two bits set? this is a compressed label (14 bit pointer offset)
            if (($length & 0xC0) === 0xC0 && isset($data[$consumed + 1])) {
                $offset = ($length & ~0xC0) << 8 | \ord($data[$consumed + 1]);
                if ($offset >= $consumed) {
                    return null;
                }

                $consumed += 2;
                $c = $this->walkQuestion($data, $offset);

                if (is_null($c)) {
                    return null;
                }
                break;
            }

            // length MUST be 0-63 (6 bits only) and data has to be large enough
            if ($length & 0xC0 || ! isset($data[$consumed + $length - 1])) {
                return null;
            }
            $consumed += $length + 1;
        }

        return $consumed;
    }
}
