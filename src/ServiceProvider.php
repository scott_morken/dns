<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 2/12/15
 * Time: 8:40 AM
 */

namespace Smorken\Dns;

use Smorken\Dns\Transports\Udp;

/**
 * Class ServiceProvider
 *
 *
 * @codeCoverageIgnore
 */
class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public function boot(): void
    {
        $this->bootConfig();
    }

    public function register(): void
    {
        $this->app->bind(\Smorken\Dns\Contracts\Query::class, function ($app) {
            $servers = $app['config']->get('dns.servers', []);
            $timeout = $app['config']->get('dns.timeout', 1.0);
            $transports = [];
            foreach ($servers as $server) {
                $transports[] = new Udp($server, $timeout);
            }
            $handler = new Handler(new Pointer);
            $q = new Query($handler, $transports);

            return $q;
        });
    }

    protected function bootConfig(): void
    {
        $config = __DIR__.'/../config/config.php';
        $this->mergeConfigFrom($config, 'dns');
        // @phpstan-ignore function.notFound
        $this->publishes([$config => config_path('dns.php')], 'config');
    }
}
