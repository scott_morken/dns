<?php

namespace Smorken\Dns;

use Smorken\Dns\Contracts\Labels\Label;
use Smorken\Dns\Contracts\Models\Answer;
use Smorken\Dns\Contracts\Models\Header;
use Smorken\Dns\Contracts\Models\Message;
use Smorken\Dns\Contracts\Models\Model;
use Smorken\Dns\Contracts\Models\Question;
use Smorken\Dns\Contracts\Parsers\Parser;
use Smorken\Dns\Factories\LabelFactory;
use Smorken\Dns\Factories\ModelFactory;
use Smorken\Dns\Factories\ParserFactory;

class Handler implements \Smorken\Dns\Contracts\Handler
{
    protected LabelFactory $labelFactory;

    protected ModelFactory $modelFactory;

    protected ParserFactory $parserFactory;

    protected int $pointer_start_index = 12;

    public function __construct(
        protected \Smorken\Dns\Contracts\Pointer $pointer

    ) {
        $this->labelFactory = new LabelFactory;
        $this->modelFactory = new ModelFactory;
        $this->parserFactory = new ParserFactory;
    }

    public function fromBinary(
        string $binary
    ): Message {
        $this->pointer->reset($this->pointer_start_index);
        $header = $this->getModel(Header::class)->newInstance(['rdata' => $binary]);
        $header = $this->getParser(ParserFactory::PARSER_HEADER)->fromBinary($header);
        $questions = $this->getQuestionsFromBinary($header->qdcount, $binary);
        $answers = $this->getAnswersFromBinary($header->ancount, $binary);

        return $this->getModel(Message::class)->newInstance([
            'header' => $header, 'questions' => $questions, 'answers' => $answers,
        ]);
    }

    /**
     * @template TTypeKey of int
     * @template TMessageTypeKey of int
     *
     * @param  TTypeKey  $which
     * @param  TMessageTypeKey  $label_id
     *
     * @phpstan-return new<LabelFactory::LABELS[TTypeKey][TMessageTypeKey]>
     */
    public function getLabel(int $label_id, int $which): Label
    {
        return $this->labelFactory->create($which, $label_id);
    }

    /**
     * @template TKey of key-of<ModelFactory::MODELS>
     *
     * @param  TKey  $model_contract
     *
     * @phpstan-return new<ModelFactory::MODELS[TKey]>
     */
    public function getModel(
        string $model_contract
    ): Model {
        return $this->modelFactory->create($model_contract);
    }

    /**
     * @template TKey of key-of<ParserFactory::PARSERS>
     *
     * @param  TKey  $parser_id
     *
     * @phpstan-return new<ParserFactory::PARSERS[TKey]>
     */
    public function getParser(
        int $parser_id
    ): Parser {
        return $this->parserFactory->create($parser_id);
    }

    public function newQuery(
        string $name,
        $type = Message::TYPE_A,
        $class = Message::CLASS_IN
    ): Message {
        $message = $this->getModel(Message::class);
        $message->header = $this->getModel(Header::class)->forQuestion();
        $message->header->qdcount = 1;
        $message->questions = [$this->getModel(Question::class)->newQuestion($name, $type, $class)];

        return $message;
    }

    public function toBinary(
        Message $message
    ): string {
        $header = $message->header;
        $questions = $message->questions;
        if ($header && ! empty($questions)) {
            $bin = $this->getParser(ParserFactory::PARSER_HEADER)->toBinary($message->header);
            $qp = $this->getParser(ParserFactory::PARSER_QUESTION);
            foreach ($message->questions as $question) {
                $this->questionNameTo($question);
                $bin .= $qp->toBinary($question);
            }

            return $bin;
        }
        throw new Exception('Header and question are required.');
    }

    protected function answerDataFrom(Answer $answer): void
    {
        $l = $this->getLabel($answer->type, LabelFactory::LABEL_ANSWER);
        $answer->data = $l->from($answer->raw_data);
    }

    protected function getAnswersFromBinary(int $count, string $binary): array
    {
        $parser = $this->getParser(ParserFactory::PARSER_ANSWER);
        $answers = [];
        for ($i = 0; $i < $count; $i++) {
            $this->pointer->next($binary, \Smorken\Dns\Contracts\Pointer::ANSWER);
            $work = substr($binary, $this->pointer->getStart());
            $a = $this->getModel(Answer::class)->newInstance(['rdata' => $work]);
            $a = $parser->fromBinary($a);
            if ($a) {
                $this->answerDataFrom($a);
                $answers[] = $a;
            }
        }

        return $answers;
    }

    protected function getQuestionsFromBinary(
        int $count,
        string $binary
    ): array {
        $parser = $this->getParser(ParserFactory::PARSER_QUESTION);
        $questions = [];
        for ($i = 0; $i < $count; $i++) {
            $this->pointer->next($binary);
            $work = substr($binary, $this->pointer->getStart());
            $q = $this->getModel(Question::class)->newInstance(['rdata' => $work]);
            $q = $parser->fromBinary($q);
            if ($q) {
                $this->questionNameFrom($q);
                $questions[] = $q;
            }
        }

        return $questions;
    }

    protected function questionNameFrom(Question $question): void
    {
        $l = $this->getLabel($question->type, LabelFactory::LABEL_QUESTION);
        $question->name = $l->from($question->name);
    }

    protected function questionNameTo(Question $question): void
    {
        $l = $this->getLabel($question->type, LabelFactory::LABEL_QUESTION);
        $question->name = $l->to($question->name);
    }
}
