<?php

namespace Smorken\Dns\Transports;

use Smorken\Dns\Exception;

class Tcp extends Base
{
    protected function checkSize(mixed $packet): void
    {
        if (\strlen((string) $packet) > 0xFFFF) {
            throw new Exception('Query failed. Packet too large for TCP transport.');
        }
    }
}
