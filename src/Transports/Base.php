<?php

namespace Smorken\Dns\Transports;

use Smorken\Dns\Contracts\Transports\Transport;
use Smorken\Dns\Exception;

abstract class Base implements Transport
{
    protected string $nameserver;

    protected string $scheme = 'udp';

    protected float $timeout;

    public function __construct(string $nameserver, float $timeout = 1.0)
    {
        $this->setNameserver($nameserver);
        $this->setTimeout($timeout);
    }

    public function send(string $packet): string
    {
        $this->checkSize($packet);
        $resource = null;
        try {
            $resource = $this->resourceCreate();
            $this->resourceSendPacket($packet, $resource);
            $response = $this->resourceReceiveResponse($resource);
        } finally {
            $this->resourceShutdown($resource);
        }

        return $response;
    }

    public function setNameserver(string $nameserver): Transport
    {
        $nameserver = $this->modifyIpv6($nameserver);
        $parts = $this->parseUrl($nameserver);
        $this->nameserver = $this->createUrlFromParts($parts);

        return $this;
    }

    public function setTimeout(float $timeout): Transport
    {
        $this->timeout = $timeout;

        return $this;
    }

    protected function checkSize(mixed $packet): void
    {
        if (isset($packet[512])) {
            throw new Exception('Query failed. Packet too large for UDP transport.');
        }
    }

    protected function createUrlFromParts(array $parts): string
    {
        return sprintf('%s://%s:%d', $this->scheme, $parts['host'], $parts['port'] ?? 53);
    }

    protected function modifyIpv6(string $nameserver): string
    {
        if (! str_contains($nameserver, '[') &&
            \substr_count($nameserver, ':') >= 2 &&
            ! str_contains($nameserver, '://')) {
            // several colons, but not enclosed in square brackets => enclose IPv6 address in square brackets
            $nameserver = '['.$nameserver.']';
        }

        return $nameserver;
    }

    protected function parseUrl(string $nameserver): array
    {
        $parts = \parse_url((! str_contains($nameserver, '://') ? $this->scheme.'://' : '').$nameserver);
        if (! isset($parts['scheme'], $parts['host']) || $parts['scheme'] !== $this->scheme || ! \filter_var(\trim($parts['host'],
            '[]'), \FILTER_VALIDATE_IP)) {
            throw new \InvalidArgumentException('Invalid nameserver address given');
        }

        return $parts;
    }

    protected function resourceCreate(): mixed
    {
        $resource = @\stream_socket_client($this->nameserver, $errno, $errstr, $this->timeout);
        if ($resource === false) {
            throw new Exception("Query failed. Unable to connect to DNS server. [$errno] $errstr");
        }
        $parts = explode('.', (string) $this->timeout);
        $part = (float) ('.'.($parts[1] ?? 0));
        $ms = (int) $part * 1000000;
        \stream_set_timeout($resource, (int) $parts[0], $ms);

        return $resource;
    }

    protected function resourceReceiveResponse(mixed $resource): string
    {
        return \fread($resource, 4096);
    }

    protected function resourceSendPacket(string $packet, mixed $resource): void
    {
        if (\fwrite($resource, $packet) === false) {
            throw new Exception('Query failed. Server closed connection.');
        }
    }

    protected function resourceShutdown(mixed $resource): void
    {
        if ($resource) {
            @\fclose($resource);
            $resource = null;
        }
    }
}
