<?php

namespace Smorken\Dns\Parsers;

use Smorken\Dns\Contracts\Models\Model;
use Smorken\Dns\Contracts\Parsers\Parser;
use Smorken\Dns\Exception;

/**
 * @implements Parser<\Smorken\Dns\Contracts\Models\Header>
 */
class Header implements Parser
{
    public function fromBinary(Model $model): ?Model
    {
        $binary = $model->rdata;
        if (strlen($binary) < 12) {
            throw new Exception('Invalid response. Length '.strlen($binary));
        }
        [$id, $fields, $qdCount, $anCount, $nsCount, $arCount] = array_values(unpack('n*', substr($binary, 0, 12)));
        $data = [
            'id' => $id,
            'rcode' => $fields & 0xF,
            'ra' => (($fields >> 7) & 1) === 1,
            'rd' => (($fields >> 8) & 1) === 1,
            'tc' => (($fields >> 9) & 1) === 1,
            'aa' => (($fields >> 10) & 1) === 1,
            'opcode' => ($fields >> 11) & 0xF,
            'qr' => (($fields >> 15) & 1) === 1,
            'qdcount' => $qdCount,
            'ancount' => $anCount,
            'nscount' => $nsCount,
            'arcount' => $arCount,
        ];

        return $model->newInstance($data);
    }

    public function toBinary(Model $model): string
    {
        $data = pack('n', $model->id);

        $flags = 0x00;
        $flags = ($flags << 1) | ($model->qr ? 1 : 0);
        $flags = ($flags << 4) | $model->opcode;
        $flags = ($flags << 1) | ($model->aa ? 1 : 0);
        $flags = ($flags << 1) | ($model->tc ? 1 : 0);
        $flags = ($flags << 1) | ($model->rd ? 1 : 0);
        $flags = ($flags << 1) | ($model->ra ? 1 : 0);
        $flags = ($flags << 3) | 0; // skip unused zero bit
        $flags = ($flags << 4) | $model->rcode;
        $data .= pack('n', $flags);
        $data .= pack('n', $model->qdcount ?: 0);
        $data .= pack('n', $model->ancount ?: 0);
        $data .= pack('n', $model->nscount ?: 0);
        $data .= pack('n', $model->arcount ?: 0);

        return $data;
    }
}
