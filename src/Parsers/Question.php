<?php

namespace Smorken\Dns\Parsers;

use Smorken\Dns\Contracts\Models\Model;
use Smorken\Dns\Contracts\Parsers\Parser;
use Smorken\Dns\Parsers\Traits\Labels;

/**
 * @implements Parser<\Smorken\Dns\Contracts\Models\Question>
 */
class Question implements Parser
{
    use Labels;

    public function fromBinary(Model $model): ?Model
    {
        return $this->fromBinaryString($model);
    }

    public function fromBinaryString(\Smorken\Dns\Contracts\Models\Question $question
    ): ?\Smorken\Dns\Contracts\Models\Question {
        $consumed = 0;
        $creator = $this->readLabels($question->rdata, $consumed);
        $labels = $creator->labels;
        $consumed = $creator->consumed;
        if (! $labels || ! isset($question->rdata[$consumed + 4 - 1])) {
            return null;
        }

        [$type, $class] = array_values(unpack('n*', substr($question->rdata, $consumed, 4)));

        return $question->newQuestion($this->labelsToName($labels), $type, $class);
    }

    public function toBinary(Model $model): string
    {
        return $this->toBinaryString($model);
    }

    public function toBinaryString(\Smorken\Dns\Contracts\Models\Question $question): string
    {
        $question = [
            $this->domainNameToBinary($question->name),
            pack('n*', $question->type, $question->class),
        ];

        return implode('', $question);
    }
}
