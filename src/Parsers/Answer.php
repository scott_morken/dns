<?php

namespace Smorken\Dns\Parsers;

use Smorken\Dns\Contracts\Models\Model;
use Smorken\Dns\Contracts\Parsers\Parser;
use Smorken\Dns\Parsers\Traits\Labels;

/**
 * @implements Parser<\Smorken\Dns\Contracts\Models\Answer>
 */
class Answer implements Parser
{
    use Labels;

    public function fromBinary(Model $model): ?Model
    {
        return $this->fromBinaryString($model);
    }

    public function fromBinaryString(\Smorken\Dns\Contracts\Models\Answer $answer
    ): ?\Smorken\Dns\Contracts\Models\Answer {
        $domain = $answer->name;
        $rdata = $answer->rdata;
        $consumed = 0;
        $len = \ord($this->nextBytes($rdata, $consumed, 1));
        // compressed label (pointer back)
        if (($len & 0xC0) === 0xC0 && isset($rdata[$consumed + 1])) {
            $consumed += 2;
        } else {
            $vo = $this->readDomain($rdata, $consumed);
            $domain = $vo->domain;
            $consumed = $vo->consumed;
        }
        if (! isset($rdata[$consumed + 10 - 1])) {
            return null;
        }

        [$type, $class] = array_values(unpack('n*', $this->nextBytes($rdata, $consumed, 4)));
        $consumed += 4;

        [$ttl] = array_values(unpack('N', $this->nextBytes($rdata, $consumed, 4)));
        $consumed += 4;

        // TTL is a UINT32 that must not have most significant bit set for BC reasons
        if ($ttl < 0 || $ttl >= 1 << 31) {
            $ttl = 0;
        }

        [$rdLength] = array_values(unpack('n', $this->nextBytes($rdata, $consumed, 2)));
        $consumed += 2;

        if (! isset($rdata[$consumed + $rdLength - 1])) {
            return null;
        }

        $data = $this->nextBytes($rdata, $consumed, $rdLength);

        return $answer->newAnswer($domain, $type, $ttl, $data, $class);
    }

    public function toBinary(Model $model): string
    {
        throw new \OutOfBoundsException('Not implemented.');
    }

    protected function nextBytes(string $binary, int $offset, int $length = 1): string
    {
        return substr($binary, $offset, $length);
    }
}
