<?php

namespace Smorken\Dns\Parsers\Traits;

use Smorken\Dns\Support\LabelsAndConsumedHelper;
use Smorken\Dns\ValueObjects\DomainVO;

trait Labels
{
    protected function domainNameToBinary(string $name): string
    {
        if ($name === '') {
            return '\0';
        }

        // break up domain name at each dot that is not preceeded by a backslash (escaped notation)
        return $this->textsToBinary(
            \array_map(
                'stripcslashes',
                \preg_split(
                    '/(?<!\\\\)\./',
                    $name.'.'
                )
            )
        );
    }

    protected function labelsToName(array $labels): string
    {
        return implode('.', $labels);
    }

    protected function readDomain(string $data, int $consumed): DomainVO
    {
        return $this->readLabels($data, $consumed)->toDomainVO();
    }

    protected function readLabels(string $data, int $consumed): LabelsAndConsumedHelper
    {
        $creator = new LabelsAndConsumedHelper($consumed);

        while (true) {
            if (! isset($data[$creator->consumed])) {
                return $creator;
            }

            $length = \ord($data[$creator->consumed]);

            // end of labels reached
            if ($length === 0) {
                $creator->incrementConsumed(1);
                break;
            }

            // first two bits set? this is a compressed label (14 bit pointer offset)
            if (($length & 0xC0) === 0xC0 && isset($data[$creator->consumed + 1])) {
                $offset = ($length & ~0xC0) << 8 | \ord($data[$creator->consumed + 1]);
                if ($offset > 0 && $offset >= $creator->consumed) {
                    return $creator->emptyCreator();
                }

                $creator->incrementConsumed(2);
                $newLabels = $this->readLabels($data, $offset)->labels;

                if (empty($newLabels)) {
                    return $creator->emptyCreator();
                }
                $creator->mergeLabels($newLabels);
                break;
            }

            // length MUST be 0-63 (6 bits only) and data has to be large enough
            if ($length & 0xC0 || ! isset($data[$creator->consumed + $length - 1])) {
                return $creator->emptyCreator();
            }

            $creator->addLabel(substr($data, $creator->consumed + 1, $length));
            $creator->incrementConsumed($length + 1);
        }

        return $creator;
    }

    /**
     * @param  string[]  $texts
     */
    protected function textsToBinary(array $texts): string
    {
        $data = '';
        foreach ($texts as $text) {
            $data .= \chr(\strlen($text)).$text;
        }

        return $data;
    }
}
