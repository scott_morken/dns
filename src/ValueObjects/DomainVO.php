<?php

declare(strict_types=1);

namespace Smorken\Dns\ValueObjects;

final class DomainVO
{
    public function __construct(
        readonly public ?string $domain,
        readonly public ?int $consumed
    ) {}
}
