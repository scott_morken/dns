<?php

declare(strict_types=1);

namespace Smorken\Dns\Factories;

use Smorken\Dns\Contracts\Models\Answer;
use Smorken\Dns\Contracts\Models\Header;
use Smorken\Dns\Contracts\Models\Message;
use Smorken\Dns\Contracts\Models\Model;
use Smorken\Dns\Contracts\Models\Question;

final class ModelFactory
{
    /**
     * @var array<class-string<Model>, class-string<Model>>
     */
    protected const MODELS = [
        Message::class => \Smorken\Dns\Models\Message::class,
        Header::class => \Smorken\Dns\Models\Header::class,
        Question::class => \Smorken\Dns\Models\Question::class,
        Answer::class => \Smorken\Dns\Models\Answer::class,
    ];

    /**
     * @template TKey of key-of<self::MODELS>
     *
     * @param  TKey  $className
     * @return new<self::MODELS[TKey]>
     */
    public function create(string $className): Model
    {
        $cls = self::MODELS[$className];

        return new $cls;
    }
}
