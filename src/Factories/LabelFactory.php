<?php

declare(strict_types=1);

namespace Smorken\Dns\Factories;

use Smorken\Dns\Contracts\Labels\Label;
use Smorken\Dns\Contracts\Models\Message;
use Smorken\Dns\Labels\Answer\Hostname;
use Smorken\Dns\Labels\Answer\IpAddress;
use Smorken\Dns\Labels\Question\PTR;
use Smorken\Dns\Labels\Question\Standard;

final class LabelFactory
{
    /**
     * @var array<int-mask-of<self::LABEL_*>, array<int, class-string<Label>>>
     */
    private const LABELS = [
        self::LABEL_ANSWER => [
            0 => \Smorken\Dns\Labels\Answer\Standard::class,
            Message::TYPE_A => IpAddress::class,
            Message::TYPE_PTR => Hostname::class,
        ],
        self::LABEL_QUESTION => [
            0 => Standard::class,
            Message::TYPE_A => Standard::class,
            Message::TYPE_PTR => PTR::class,
        ],
    ];

    public const LABEL_ANSWER = 1;

    public const LABEL_QUESTION = 0;

    /**
     * @template TTypeKey of int
     * @template TMessageTypeKey of int
     *
     * @param  TTypeKey  $type
     * @param  TMessageTypeKey  $messageType
     *
     * @phpstan-return new<self::LABELS[TTypeKey][TMessageTypeKey]>
     */
    public function create(int $type, int $messageType): Label
    {
        $cls = self::LABELS[$type][$messageType] ?? throw new \InvalidArgumentException('Invalid type and/or message type');

        return new $cls;
    }
}
