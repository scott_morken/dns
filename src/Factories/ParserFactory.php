<?php

declare(strict_types=1);

namespace Smorken\Dns\Factories;

use Smorken\Dns\Contracts\Parsers\Parser;

final class ParserFactory
{
    /**
     * @var array<int, class-string<Parser>>
     */
    private const PARSERS = [
        self::PARSER_HEADER => \Smorken\Dns\Parsers\Header::class,
        self::PARSER_QUESTION => \Smorken\Dns\Parsers\Question::class,
        self::PARSER_ANSWER => \Smorken\Dns\Parsers\Answer::class,
    ];

    public const PARSER_ANSWER = 2;

    public const PARSER_HEADER = 0;

    public const PARSER_QUESTION = 1;

    /**
     * @template TKey of key-of<self::PARSERS>
     *
     * @phpstan-param TKey $type
     *
     * @phpstan-return new<self::PARSERS[TKey]>
     */
    public function create(int $type): Parser
    {
        $cls = self::PARSERS[$type];

        return new $cls;
    }
}
