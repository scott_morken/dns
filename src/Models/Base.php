<?php

namespace Smorken\Dns\Models;

use Smorken\Dns\Contracts\Models\Model;

#[\AllowDynamicProperties]
abstract class Base implements Model
{
    protected array $attributes = [];

    public function __construct(array $attrs = [])
    {
        $this->setAttributes($attrs);
    }

    public function __get(string $name): mixed
    {
        return $this->getAttribute($name);
    }

    public function __set(string $name, mixed $value): void
    {
        $this->setAttribute($name, $value);
    }

    public function getAttribute(string $attr): mixed
    {
        return $this->attributes[$attr] ?? null;
    }

    public function newInstance(array $attrs = []): static
    {
        return new static($attrs);
    }

    public function setAttribute(string $attr, $value): static
    {
        $this->attributes[$attr] = $value;

        return $this;
    }

    public function setAttributes(array $attrs = []): static
    {
        foreach ($attrs as $k => $v) {
            $this->setAttribute($k, $v);
        }

        return $this;
    }

    public function toArray(): array
    {
        return $this->attributes;
    }
}
