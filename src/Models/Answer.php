<?php

namespace Smorken\Dns\Models;

class Answer extends Base implements \Smorken\Dns\Contracts\Models\Answer
{
    protected array $attributes = [
        'ttl' => 0,
        'name' => '',
        'type' => 0,
        'class' => \Smorken\Dns\Contracts\Models\Message::CLASS_IN,
        'data' => '',
        'raw_data' => '',
        'rdata' => '',
    ];

    public function newAnswer(
        string $name,
        int $type,
        int $ttl,
        string $data,
        int $class = \Smorken\Dns\Contracts\Models\Message::CLASS_IN
    ): Answer {
        return $this->newInstance([
            'name' => $name,
            'type' => $type,
            'ttl' => $ttl,
            'raw_data' => $data,
            'class' => $class,
        ]);
    }
}
