<?php

namespace Smorken\Dns\Models;

use Smorken\Dns\Contracts\Models\Message;

class Question extends Base implements \Smorken\Dns\Contracts\Models\Question
{
    protected array $attributes = [
        'name' => '',
        'type' => Message::TYPE_A,
        'class' => Message::CLASS_IN,
        'rdata' => null,
    ];

    public function newQuestion(
        string $name,
        int $type = Message::TYPE_A,
        int $class = Message::CLASS_IN
    ): Question {
        return new static(['name' => $name, 'type' => $type, 'class' => $class]);
    }
}
