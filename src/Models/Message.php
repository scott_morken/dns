<?php

namespace Smorken\Dns\Models;

class Message extends Base implements \Smorken\Dns\Contracts\Models\Message
{
    protected array $attributes = [
        'header' => null,
        'questions' => [],
        'answers' => [],
    ];

    public function firstAnswer(): string|array|null
    {
        if (isset($this->answers[0])) {
            return $this->answers[0]->data;
        }

        return null;
    }
}
