<?php

namespace Smorken\Dns\Models;

use Smorken\Dns\Contracts\Models\Message;

class Header extends Base implements \Smorken\Dns\Contracts\Models\Header
{
    protected array $attributes = [
        'id' => 0,
        'aa' => false,
        'opcode' => Message::OPCODE_QUERY,
        'qr' => false,
        'ra' => false,
        'rcode' => Message::RCODE_OK,
        'rd' => false,
        'tc' => false,
        'qdcount' => 0,
        'ancount' => 0,
        'nscount' => 0,
        'arcount' => 0,
    ];

    public function forQuestion(array $attrs = []): Header
    {
        $attrs['qr'] = false;
        $attrs['rd'] = true;
        $attrs['qdcount'] = 1;
        $h = $this->newInstance($attrs);
        $h->generateId();

        return $h;
    }

    /**
     * @throws \Exception
     */
    public function generateId(): self
    {
        $this->id = $this->getRandomId();

        return $this;
    }

    protected function getRandomId(): int
    {
        if (function_exists('random_int')) {
            return random_int(0, 0xFFFF);
        }

        return mt_rand(0, 0xFFFF);
    }
}
