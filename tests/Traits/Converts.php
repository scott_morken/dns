<?php

namespace Tests\Smorken\Dns\Traits;

trait Converts
{
    protected function convertBinaryToHexDump($input)
    {
        return $this->formatHexDump(implode('', unpack('H*', $input)));
    }

    protected function convertHexDumpToBinary($input)
    {
        return pack('H*', str_replace(' ', '', $input));
    }

    protected function formatHexDump($input)
    {
        return implode(' ', str_split(str_replace(' ', '', $input), 2));
    }
}
