<?php

namespace Tests\Smorken\Dns\Integration\LabelledQuestions;

use PHPUnit\Framework\TestCase;
use Smorken\Dns\Contracts\Models\Message;
use Smorken\Dns\Labels\Question\PTR;
use Smorken\Dns\Labels\Question\Standard;
use Smorken\Dns\Models\Question;
use Smorken\Dns\Pointer;
use Tests\Smorken\Dns\Traits\Converts;

class PTRTest extends TestCase
{
    use Converts;

    public function testQuestionFromBinary()
    {
        $data = [
            '01 34 01 34 01 38 01 38 07 69 6e', // 4.4.8.8.in-addr.arpa
            '2d 61 64 64 72 04 61 72 70 61 00',
            '00 0c 00 01', // type PTR, class IN
        ];
        $bin = $this->convertHexDumpToBinary($this->formatHexDump(implode(' ', $data)));
        $question = new Question(['rdata' => $bin]);
        $parser = new \Smorken\Dns\Parsers\Question;
        $q = $parser->fromBinary($question);
        $q->name = (new PTR)->from($q->name);
        $this->assertSame('8.8.4.4', $q->name);
        $this->assertSame(Message::TYPE_PTR, $q->type);
        $this->assertSame(Message::CLASS_IN, $q->class);
    }

    public function testQuestionToBinary()
    {
        $data = [
            '01 34 01 34 01 38 01 38 07 69 6e', // 4.4.8.8.in-addr.arpa
            '2d 61 64 64 72 04 61 72 70 61 00',
            '00 0c 00 01', // type PTR, class IN
        ];
        $expected = $this->formatHexDump(implode(' ', $data));
        $name = (new PTR)->to('8.8.4.4');
        $question = (new Question)->newQuestion($name, Message::TYPE_PTR);
        $parser = new \Smorken\Dns\Parsers\Question;
        $bin = $parser->toBinary($question);
        $result = $this->convertBinaryToHexDump($bin);
        $this->assertSame($expected, $result);
    }

    public function testTwoQuestionsFromBinaryAdvancesIndex()
    {
        $data = [
            '01 34 01 34 01 38 01 38 07 69 6e', // 4.4.8.8.in-addr.arpa
            '2d 61 64 64 72 04 61 72 70 61 00',
            '00 0c 00 01', // type PTR, class IN
            '04 69 67 6f 72 02 69 6f 00', // igor.io
            '00 01 00 01', // type A, class IN
        ];
        $bin = $this->convertHexDumpToBinary($this->formatHexDump(implode(' ', $data)));
        $pointer = new Pointer;
        $pointer->next($bin);
        $question = new Question(['rdata' => substr($bin, $pointer->getStart())]);
        $parser = new \Smorken\Dns\Parsers\Question;
        $q = $parser->fromBinary($question);
        $this->assertSame('8.8.4.4', (new PTR)->from($q->name));
        $this->assertSame(Message::TYPE_PTR, $q->type);
        $this->assertSame(Message::CLASS_IN, $q->class);
        $pointer->next($bin);
        $question = new Question(['rdata' => substr($bin, $pointer->getStart())]);
        $q = $parser->fromBinary($question);
        $this->assertSame('igor.io', (new Standard)->from($q->name));
        $this->assertSame(Message::TYPE_A, $q->type);
        $this->assertSame(Message::CLASS_IN, $q->class);
    }
}
