<?php

namespace Tests\Smorken\Dns\Integration\LabelledAnswers;

use PHPUnit\Framework\TestCase;
use Smorken\Dns\Contracts\Models\Message;
use Smorken\Dns\Labels\Answer\Hostname;
use Smorken\Dns\Labels\Answer\IpAddress;
use Smorken\Dns\Models\Answer;
use Smorken\Dns\Pointer;
use Tests\Smorken\Dns\Traits\Converts;

class PTRTest extends TestCase
{
    use Converts;

    public function testAnswerFromBinary()
    {
        $data = [
            'c0 0c', // answser, offset pointer to rdata
            '00 0c 00 01', // answer, type PTR, class IN
            '00 01 51 7f', // answer, ttl
            '00 20', // answer, rdlength 32
            '13 67 6f 6f 67 6c 65 2d 70 75 62 6c 69 63 2d 64', // answer, rdata google-public-dns-b.google.com
            '6e 73 2d 62 06 67 6f 6f 67 6c 65 03 63 6f 6d 00',
        ];
        $bin = $this->convertHexDumpToBinary($this->formatHexDump(implode(' ', $data)));
        $answer = new Answer(['rdata' => $bin]);
        $parser = new \Smorken\Dns\Parsers\Answer;
        $a = $parser->fromBinary($answer);
        $a->data = (new Hostname)->from($a->raw_data);
        $this->assertSame('google-public-dns-b.google.com', $a->data);
        $this->assertSame(Message::TYPE_PTR, $a->type);
        $this->assertSame(Message::CLASS_IN, $a->class);
    }

    public function testTwoAnswersFromBinaryAdvancesIndex()
    {
        $data = [
            'c0 0c', // answser, offset pointer to rdata
            '00 0c 00 01', // answer, type PTR, class IN
            '00 01 51 7f', // answer, ttl
            '00 20', // answer, rdlength 32
            '13 67 6f 6f 67 6c 65 2d 70 75 62 6c 69 63 2d 64', // answer, rdata google-public-dns-b.google.com
            '6e 73 2d 62 06 67 6f 6f 67 6c 65 03 63 6f 6d 00',
            'c0 32', // answer, offset pointer
            '00 01 00 01', // answer, type A, class IN
            '00 00 0d f7', // answer, ttl 3575,
            '00 04', // answer, rdlength 4
            'c1 df 4e 98', //answer, rdata 193.223.78.152
        ];
        $bin = $this->convertHexDumpToBinary($this->formatHexDump(implode(' ', $data)));
        $pointer = new Pointer;
        $pointer->next($bin, \Smorken\Dns\Contracts\Pointer::ANSWER);
        $answer = new Answer(['rdata' => $bin]);
        $parser = new \Smorken\Dns\Parsers\Answer;
        $a = $parser->fromBinary($answer);
        $a->data = (new Hostname)->from($a->raw_data);
        $this->assertSame('google-public-dns-b.google.com', $a->data);
        $this->assertSame(Message::TYPE_PTR, $a->type);
        $this->assertSame(Message::CLASS_IN, $a->class);
        $pointer->next($bin, \Smorken\Dns\Contracts\Pointer::ANSWER);
        $answer = new Answer(['rdata' => substr($bin, $pointer->getStart())]);
        $a = $parser->fromBinary($answer);
        $a->data = (new IpAddress)->from($a->raw_data);
        $this->assertSame('193.223.78.152', $a->data);
        $this->assertSame(Message::TYPE_A, $a->type);
        $this->assertSame(Message::CLASS_IN, $a->class);
    }
}
