<?php

namespace Tests\Smorken\Dns\Unit\Models;

use PHPUnit\Framework\TestCase;
use Smorken\Dns\Contracts\Models\Message;
use Smorken\Dns\Models\Header;

class HeaderTest extends TestCase
{
    public function testCreateQuestion()
    {
        $header = (new Header)->forQuestion();
        $this->assertIsInt($header->id);
        $this->assertFalse($header->qr);
        $this->assertTrue($header->rd);
    }

    public function testDefaults()
    {
        $header = new Header;
        $expected = [
            'id' => 0,
            'aa' => false,
            'opcode' => Message::OPCODE_QUERY,
            'qr' => false,
            'ra' => false,
            'rcode' => Message::RCODE_OK,
            'rd' => false,
            'tc' => false,
            'qdcount' => 0,
            'ancount' => 0,
            'nscount' => 0,
            'arcount' => 0,
        ];
        $this->assertEquals($expected, $header->toArray());
    }

    public function testHeaderCanCreateRandomId()
    {
        $header = (new Header)->generateId();
        $this->assertIsInt($header->id);
    }
}
