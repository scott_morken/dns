<?php

namespace Tests\Smorken\Dns\Unit;

use PHPUnit\Framework\TestCase;
use Smorken\Dns\Contracts\Query;
use Smorken\Dns\Factory;

class FactoryTest extends TestCase
{
    public function testCreatesQuery()
    {
        $q = Factory::makeQuery();
        $this->assertInstanceOf(Query::class, $q);
    }
}
