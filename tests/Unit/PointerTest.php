<?php

namespace Tests\Smorken\Dns\Unit;

use PHPUnit\Framework\TestCase;
use Smorken\Dns\Pointer;
use Tests\Smorken\Dns\Traits\Converts;

class PointerTest extends TestCase
{
    use Converts;

    public function testFindOneAnswerInBinary()
    {
        $data = [
            '72 62 01 00 00 01 00 00 00 00 00 00', // header
            '01 34 01 34 01 38 01 38 07 69 6e', // 4.4.8.8.in-addr.arpa
            '2d 61 64 64 72 04 61 72 70 61 00',
            '00 0c 00 01', // type PTR, class IN
            'c0 0c', // answer, offset pointer to 4.4.8.8...
            '00 0c 00 01', // answer, type PTR, class IN
            '00 01 51 7f', // answer, ttl 86399
            '00 20', // answer, rdlength 32
            '13 67 6f 6f 67 6c 65 2d 70 75 62 6c 69 63 2d 64', // answer, rdata google-public-dns-b.google.com
            '6e 73 2d 62 06 67 6f 6f 67 6c 65 03 63 6f 6d 00',
        ];
        $bin = $this->convertHexDumpToBinary($this->formatHexDump(implode(' ', $data)));
        $sut = new Pointer(12);
        $sut->next($bin);
        $this->assertEquals(12, $sut->getStart());
        $this->assertEquals(38, $sut->getCurrent());
        $sut->next($bin, \Smorken\Dns\Contracts\Pointer::ANSWER);
        $this->assertEquals(38, $sut->getStart());
        $this->assertEquals(strlen($bin), $sut->getCurrent());
    }

    public function testFindOneQuestionInBinary()
    {
        $data = [
            '72 62 01 00 00 01 00 00 00 00 00 00', // header
            '01 34 01 34 01 38 01 38 07 69 6e', // 4.4.8.8.in-addr.arpa
            '2d 61 64 64 72 04 61 72 70 61 00',
            '00 0c 00 01', // type PTR, class IN
        ];
        $bin = $this->convertHexDumpToBinary($this->formatHexDump(implode(' ', $data)));
        $sut = new Pointer(12);
        $sut->next($bin);
        $this->assertEquals(12, $sut->getStart());
        $this->assertEquals(strlen($bin), $sut->getCurrent());
    }

    public function testFindTwoAnswersInBinary()
    {
        $data = [
            '72 62 01 00 00 01 00 00 00 00 00 00', // header
            '01 34 01 34 01 38 01 38 07 69 6e', // 4.4.8.8.in-addr.arpa
            '2d 61 64 64 72 04 61 72 70 61 00',
            '00 0c 00 01', // type PTR, class IN
            'c0 0c', // answer, offset pointer to 4.4.8.8...
            '00 0c 00 01', // answer, type PTR, class IN
            '00 01 51 7f', // answer, ttl 86399
            '00 20', // answer, rdlength 32
            '13 67 6f 6f 67 6c 65 2d 70 75 62 6c 69 63 2d 64', // answer, rdata google-public-dns-b.google.com
            '6e 73 2d 62 06 67 6f 6f 67 6c 65 03 63 6f 6d 00',
            'c0 32', // answer, offset pointer
            '00 01 00 01', // answer, type CNAME, class IN
            '00 00 0d f7', // answer, ttl 3575,
            '00 04', // answer, rdlength 4
            'c1 df 4e 98', //answer, rdata 193.223.78.152
        ];
        $bin = $this->convertHexDumpToBinary($this->formatHexDump(implode(' ', $data)));
        $sut = new Pointer(12);
        $sut->next($bin);
        $this->assertEquals(12, $sut->getStart());
        $this->assertEquals(38, $sut->getCurrent());
        $sut->next($bin, \Smorken\Dns\Contracts\Pointer::ANSWER);
        $this->assertEquals(38, $sut->getStart());
        $this->assertEquals(82, $sut->getCurrent());
        $sut->next($bin, \Smorken\Dns\Contracts\Pointer::ANSWER);
        $this->assertEquals(82, $sut->getStart());
        $this->assertEquals(strlen($bin), $sut->getCurrent());
    }

    public function testTwoQuestionsInBinaryAdvancesIndex()
    {
        $data = [
            '72 62 01 00 00 01 00 00 00 00 00 00', // header
            '01 34 01 34 01 38 01 38 07 69 6e', // 4.4.8.8.in-addr.arpa
            '2d 61 64 64 72 04 61 72 70 61 00',
            '00 0c 00 01', // type PTR, class IN
            '04 69 67 6f 72 02 69 6f 00', // igor.io
            '00 01 00 01', // type A, class IN
        ];
        $bin = $this->convertHexDumpToBinary($this->formatHexDump(implode(' ', $data)));
        $sut = new Pointer(12);
        $sut->next($bin);
        $this->assertEquals(12, $sut->getStart());
        $this->assertEquals(38, $sut->getCurrent());
        $sut->next($bin);
        $this->assertEquals(38, $sut->getStart());
        $this->assertEquals(strlen($bin), $sut->getCurrent());
    }
}
