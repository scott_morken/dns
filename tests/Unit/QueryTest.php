<?php

namespace Tests\Smorken\Dns\Unit;

use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Dns\Contracts\Models\Message;
use Smorken\Dns\Contracts\Transports\Transport;
use Smorken\Dns\Handler;
use Smorken\Dns\Pointer;
use Smorken\Dns\Query;
use Tests\Smorken\Dns\Traits\Converts;

class QueryTest extends TestCase
{
    use Converts;

    public function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }

    public function testPTRRecordHasAnswer()
    {
        $question = [
            '72 62 01 00 00 01 00 00 00 00 00 00', // header
            '01 34 01 34 01 38 01 38 07 69 6e', // 4.4.8.8.in-addr.arpa
            '2d 61 64 64 72 04 61 72 70 61 00',
            '00 0c 00 01', // type PTR, class IN
        ];
        $answer = [
            '72 62 81 80 00 01 00 01 00 00 00 00', // header
            '01 34 01 34 01 38 01 38 07 69 6e', // 4.4.8.8.in-addr.arpa
            '2d 61 64 64 72 04 61 72 70 61 00',
            '00 0c 00 01', // type PTR, class IN
            'c0 0c', // answser, offset pointer to rdata
            '00 0c 00 01', // answer, type PTR, class IN
            '00 01 51 7f', // answer, ttl
            '00 20', // answer, rdlength 32
            '13 67 6f 6f 67 6c 65 2d 70 75 62 6c 69 63 2d 64', // answer, rdata google-public-dns-b.google.com
            '6e 73 2d 62 06 67 6f 6f 67 6c 65 03 63 6f 6d 00',
        ];
        $question_bin = $this->convertHexDumpToBinary($this->formatHexDump(implode(' ', $question)));
        $answer_bin = $this->convertHexDumpToBinary($this->formatHexDump(implode(' ', $answer)));
        [$sut, $transports] = $this->getSut();
        $transports[0]->shouldReceive('send')->once()->with(m::on(function ($arg) use ($question_bin) {
            return substr($question_bin, 4) === substr($arg, 4);
        }))->andReturn($answer_bin);
        $message = $sut->query('8.8.4.4', Message::TYPE_PTR);
        $h = $message->header;
        $this->assertSame(0x7262, $h->id);
        $this->assertTrue($h->qr);
        $this->assertSame(Message::OPCODE_QUERY, $h->opcode);
        $this->assertFalse($h->aa);
        $this->assertFalse($h->tc);
        $this->assertTrue($h->rd);
        $this->assertTrue($h->ra);
        $this->assertSame(Message::RCODE_OK, $h->rcode);
        $this->assertSame(1, $h->qdcount);
        $q = $message->questions[0];
        $this->assertSame('8.8.4.4', $q->name);
        $this->assertSame(Message::TYPE_PTR, $q->type);
        $this->assertSame(Message::CLASS_IN, $q->class);
        $this->assertSame(1, $h->ancount);
        $a = $message->answers[0];
        $this->assertSame('google-public-dns-b.google.com', $message->firstAnswer());
        $this->assertSame('google-public-dns-b.google.com', $a->data);
        $this->assertSame(Message::TYPE_PTR, $a->type);
        $this->assertSame(Message::CLASS_IN, $a->class);
    }

    public function testPTRRecordWithoutAnswer()
    {
        $question = [
            '72 62 01 00 00 01 00 00 00 00 00 00', // header
            '01 34 01 34 01 38 01 38 07 69 6e', // 4.4.8.8.in-addr.arpa
            '2d 61 64 64 72 04 61 72 70 61 00',
            '00 0c 00 01', // type PTR, class IN
        ];
        $answer = [
            '72 62 81 80 00 01 00 00 00 00 00 00', // header
            '01 34 01 34 01 38 01 38 07 69 6e', // 4.4.8.8.in-addr.arpa
            '2d 61 64 64 72 04 61 72 70 61 00',
            '00 0c 00 01', // type PTR, class IN
        ];
        $question_bin = $this->convertHexDumpToBinary($this->formatHexDump(implode(' ', $question)));
        $answer_bin = $this->convertHexDumpToBinary($this->formatHexDump(implode(' ', $answer)));
        [$sut, $transports] = $this->getSut();
        $transports[0]->shouldReceive('send')->once()->with(m::on(function ($arg) use ($question_bin) {
            return substr($question_bin, 4) === substr($arg, 4);
        }))->andReturn($answer_bin);
        $message = $sut->query('8.8.4.4', Message::TYPE_PTR);
        $h = $message->header;
        $this->assertSame(0x7262, $h->id);
        $this->assertTrue($h->qr);
        $this->assertSame(Message::OPCODE_QUERY, $h->opcode);
        $this->assertFalse($h->aa);
        $this->assertFalse($h->tc);
        $this->assertTrue($h->rd);
        $this->assertTrue($h->ra);
        $this->assertSame(Message::RCODE_OK, $h->rcode);
        $this->assertSame(1, $h->qdcount);
        $q = $message->questions[0];
        $this->assertSame('8.8.4.4', $q->name);
        $this->assertSame(Message::TYPE_PTR, $q->type);
        $this->assertSame(Message::CLASS_IN, $q->class);
        $this->assertSame(0, $h->ancount);
        $this->assertCount(0, $message->answers);
        $this->assertNull($message->firstAnswer());
    }

    public function testPTRRecordInvalidAnswer()
    {
        $question = [
            '72 62 01 00 00 01 00 00 00 00 00 00', // header
            '01 34 01 34 01 38 01 38 07 69 6e', // 4.4.8.8.in-addr.arpa
            '2d 61 64 64 72 04 61 72 70 61 00',
            '00 0c 00 01', // type PTR, class IN
        ];
        $answer = [
            '72 62 81 80 00 01 00 01 00 00 00 00', // header
            '01 34 01 34 01 38 01 38 07 69 6e', // 4.4.8.8.in-addr.arpa
            '2d 61 64 64 72 04 61 72 70 61 00',
            '00 0c 00 01', // type PTR, class IN
            'c0 0c', // answser, offset pointer to rdata
        ];
        $question_bin = $this->convertHexDumpToBinary($this->formatHexDump(implode(' ', $question)));
        $answer_bin = $this->convertHexDumpToBinary($this->formatHexDump(implode(' ', $answer)));
        [$sut, $transports] = $this->getSut();
        $transports[0]->shouldReceive('send')->once()->with(m::on(function ($arg) use ($question_bin) {
            return substr($question_bin, 4) === substr($arg, 4);
        }))->andReturn($answer_bin);
        $message = $sut->query('8.8.4.4', Message::TYPE_PTR);
        $h = $message->header;
        $this->assertSame(0x7262, $h->id);
        $this->assertTrue($h->qr);
        $this->assertSame(Message::OPCODE_QUERY, $h->opcode);
        $this->assertFalse($h->aa);
        $this->assertFalse($h->tc);
        $this->assertTrue($h->rd);
        $this->assertTrue($h->ra);
        $this->assertSame(Message::RCODE_OK, $h->rcode);
        $this->assertSame(1, $h->qdcount);
        $q = $message->questions[0];
        $this->assertSame('8.8.4.4', $q->name);
        $this->assertSame(Message::TYPE_PTR, $q->type);
        $this->assertSame(Message::CLASS_IN, $q->class);
        $this->assertSame(1, $h->ancount);
        $this->assertCount(0, $message->answers);
    }

    protected function getSut(array $transports = [])
    {
        if (empty($transports)) {
            $transports = [m::mock(Transport::class)];
        }
        $h = new Handler(new Pointer);
        $sut = new Query($h, $transports);

        return [$sut, $transports];
    }
}
