<?php

namespace Tests\Smorken\Dns\Unit;

use PHPUnit\Framework\TestCase;
use Smorken\Dns\Contracts\Models\Message;
use Smorken\Dns\Handler;
use Smorken\Dns\Pointer;
use Tests\Smorken\Dns\Traits\Converts;

class HandlerTest extends TestCase
{
    use Converts;

    public function testAQuestionMessageFromBinary()
    {
        $data = [
            '72 62 01 00 00 01 00 00 00 00 00 00', // header
            '04 69 67 6f 72 02 69 6f 00', // igor.io
            '00 01 00 01', // type A, class IN
        ];
        $bin = $this->convertHexDumpToBinary($this->formatHexDump(implode(' ', $data)));
        $sut = $this->getSut();
        $message = $sut->fromBinary($bin);
        $h = $message->header;
        $this->assertSame(0x7262, $h->id);
        $this->assertFalse($h->qr);
        $this->assertSame(Message::OPCODE_QUERY, $h->opcode);
        $this->assertFalse($h->aa);
        $this->assertFalse($h->tc);
        $this->assertTrue($h->rd);
        $this->assertFalse($h->ra);
        $this->assertSame(Message::RCODE_OK, $h->rcode);
        $this->assertSame(1, $h->qdcount);
        $this->assertSame(0, $h->ancount);
        $this->assertCount(1, $message->questions);
        $q = $message->questions[0];
        $this->assertSame('igor.io', $q->name);
        $this->assertSame(Message::TYPE_A, $q->type);
        $this->assertSame(Message::CLASS_IN, $q->class);
    }

    public function testAQuestionMessageToBinary()
    {
        $data = [
            '72 62 01 00 00 01 00 00 00 00 00 00', // header
            '04 69 67 6f 72 02 69 6f 00', // igor.io
            '00 01 00 01', // type A, class IN
        ];
        $expected = substr($this->formatHexDump(implode(' ', $data)), 6);
        $sut = $this->getSut();
        $message = $sut->newQuery('igor.io', Message::TYPE_A);
        $bin = $sut->toBinary($message);
        $result = substr($this->convertBinaryToHexDump($bin), 6);
        $this->assertSame($expected, $result);
    }

    public function testPTRQuestionMessageFromBinary()
    {
        $data = [
            '72 62 01 00 00 01 00 00 00 00 00 00', // header
            '01 34 01 34 01 38 01 38 07 69 6e', // 4.4.8.8.in-addr.arpa
            '2d 61 64 64 72 04 61 72 70 61 00',
            '00 0c 00 01', // type PTR, class IN
        ];
        $bin = $this->convertHexDumpToBinary($this->formatHexDump(implode(' ', $data)));
        $sut = $this->getSut();
        $message = $sut->fromBinary($bin);
        $h = $message->header;
        $this->assertSame(0x7262, $h->id);
        $this->assertFalse($h->qr);
        $this->assertSame(Message::OPCODE_QUERY, $h->opcode);
        $this->assertFalse($h->aa);
        $this->assertFalse($h->tc);
        $this->assertTrue($h->rd);
        $this->assertFalse($h->ra);
        $this->assertSame(Message::RCODE_OK, $h->rcode);
        $this->assertSame(1, $h->qdcount);
        $this->assertSame(0, $h->ancount);
        $this->assertCount(1, $message->questions);
        $q = $message->questions[0];
        $this->assertSame('8.8.4.4', $q->name);
        $this->assertSame(Message::TYPE_PTR, $q->type);
        $this->assertSame(Message::CLASS_IN, $q->class);
    }

    public function testPTRQuestionMessageToBinary()
    {
        $data = [
            '72 62 01 00 00 01 00 00 00 00 00 00', // header
            '01 34 01 34 01 38 01 38 07 69 6e', // 4.4.8.8.in-addr.arpa
            '2d 61 64 64 72 04 61 72 70 61 00',
            '00 0c 00 01', // type PTR, class IN
        ];
        $expected = substr($this->formatHexDump(implode(' ', $data)), 6);
        $sut = $this->getSut();
        $message = $sut->newQuery('8.8.4.4', Message::TYPE_PTR);
        $bin = $sut->toBinary($message);
        $result = substr($this->convertBinaryToHexDump($bin), 6);
        $this->assertSame($expected, $result);
    }

    public function testPTRResponseMessageFromBinary()
    {
        $data = [
            '72 62 81 80 00 01 00 01 00 00 00 00', // header
            '01 34 01 34 01 38 01 38 07 69 6e', // 4.4.8.8.in-addr.arpa
            '2d 61 64 64 72 04 61 72 70 61 00',
            '00 0c 00 01', // type PTR, class IN
            'c0 0c', // answser, offset pointer to rdata
            '00 0c 00 01', // answer, type PTR, class IN
            '00 01 51 7f', // answer, ttl
            '00 20', // answer, rdlength 32
            '13 67 6f 6f 67 6c 65 2d 70 75 62 6c 69 63 2d 64', // answer, rdata google-public-dns-b.google.com
            '6e 73 2d 62 06 67 6f 6f 67 6c 65 03 63 6f 6d 00',
        ];
        $bin = $this->convertHexDumpToBinary($this->formatHexDump(implode(' ', $data)));
        $sut = $this->getSut();
        $message = $sut->fromBinary($bin);
        $h = $message->header;
        $this->assertSame(0x7262, $h->id);
        $this->assertTrue($h->qr);
        $this->assertSame(Message::OPCODE_QUERY, $h->opcode);
        $this->assertFalse($h->aa);
        $this->assertFalse($h->tc);
        $this->assertTrue($h->rd);
        $this->assertTrue($h->ra);
        $this->assertSame(Message::RCODE_OK, $h->rcode);
        $this->assertSame(1, $h->qdcount);
        $q = $message->questions[0];
        $this->assertSame('8.8.4.4', $q->name);
        $this->assertSame(Message::TYPE_PTR, $q->type);
        $this->assertSame(Message::CLASS_IN, $q->class);
        $this->assertSame(1, $h->ancount);
        $a = $message->answers[0];
        $this->assertSame('google-public-dns-b.google.com', $message->firstAnswer());
        $this->assertSame('google-public-dns-b.google.com', $a->data);
        $this->assertSame(Message::TYPE_PTR, $a->type);
        $this->assertSame(Message::CLASS_IN, $a->class);
    }

    public function testPTRResponseMessageFromBinaryWithoutCompressedLabel()
    {
        $data = [
            '1e 2a 85 00 00 01 00 01 00 01 00 00',
            '01 32 01 30 02 34 33 03 31 37 32 07 69 6e 2d 61 64 64 72 04 61 72 70 61 00',
            '00 0c 00 01',
            '01 32',
            '01 30 02 34',
            '33 03',
            '31 37 32 07 69 6e 2d 61 64 64 72 04 61 72 70 61 00 00 0c 00 01 00 09 3a 80 00 12 05 6e 67',
            '69 6e 78 03 61 70 70 06 64 6f 63 6b 65 72 00 02 34 33 03 31 37 32 07 69 6e 2d 61 64 64 72 04',
            '61 72 70 61 00 00 02 00 01 00 09 3a 80 00 10 03 64 6e 73 03 61 70 70 06 64 6f 63 6b 65 72 00',
        ];
        $bin = $this->convertHexDumpToBinary($this->formatHexDump(implode(' ', $data)));
        $sut = $this->getSut();
        $message = $sut->fromBinary($bin);
        $h = $message->header;
        $this->assertTrue($h->qr);
        $this->assertSame(Message::OPCODE_QUERY, $h->opcode);
        $this->assertTrue($h->aa);
        $this->assertFalse($h->tc);
        $this->assertTrue($h->rd);
        $this->assertFalse($h->ra);
        $this->assertSame(Message::RCODE_OK, $h->rcode);
        $this->assertSame(1, $h->qdcount);
        $q = $message->questions[0];
        $this->assertSame('172.43.0.2', $q->name);
        $this->assertSame(Message::TYPE_PTR, $q->type);
        $this->assertSame(Message::CLASS_IN, $q->class);
        $this->assertSame(1, $h->ancount);
        $a = $message->answers[0];
        $this->assertSame('nginx.app.docker', $message->firstAnswer());
    }

    public function testPTRTwoQuestionMessageFromBinary()
    {
        $data = [
            '72 62 01 00 00 02 00 00 00 00 00 00', // header
            '01 34 01 34 01 38 01 38 07 69 6e', // 4.4.8.8.in-addr.arpa
            '2d 61 64 64 72 04 61 72 70 61 00',
            '00 0c 00 01', // type PTR, class IN
            '04 69 67 6f 72 02 69 6f 00', // igor.io
            '00 01 00 01', // type A, class IN
        ];
        $bin = $this->convertHexDumpToBinary($this->formatHexDump(implode(' ', $data)));
        $sut = $this->getSut();
        $message = $sut->fromBinary($bin);
        $h = $message->header;
        $this->assertSame(0x7262, $h->id);
        $this->assertFalse($h->qr);
        $this->assertSame(Message::OPCODE_QUERY, $h->opcode);
        $this->assertFalse($h->aa);
        $this->assertFalse($h->tc);
        $this->assertTrue($h->rd);
        $this->assertFalse($h->ra);
        $this->assertSame(Message::RCODE_OK, $h->rcode);
        $this->assertSame(2, $h->qdcount);
        $this->assertSame(0, $h->ancount);
        $this->assertCount(2, $message->questions);
        $q = $message->questions[0];
        $this->assertSame('8.8.4.4', $q->name);
        $this->assertSame(Message::TYPE_PTR, $q->type);
        $this->assertSame(Message::CLASS_IN, $q->class);
        $q = $message->questions[1];
        $this->assertSame('igor.io', $q->name);
        $this->assertSame(Message::TYPE_A, $q->type);
        $this->assertSame(Message::CLASS_IN, $q->class);
    }

    protected function getSut(): \Smorken\Dns\Contracts\Handler
    {
        return new Handler(new Pointer);
    }
}
