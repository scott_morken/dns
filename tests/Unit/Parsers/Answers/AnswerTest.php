<?php

namespace Tests\Smorken\Dns\Unit\Parsers\Answers;

use PHPUnit\Framework\TestCase;
use Smorken\Dns\Contracts\Models\Message;
use Smorken\Dns\Models\Answer;
use Tests\Smorken\Dns\Traits\Converts;

class AnswerTest extends TestCase
{
    use Converts;

    public function testAAnswerFromBinary()
    {
        $data = [
            'c0 0c', // answer, offset pointer to igor.io
            '00 01 00 01', // answer, type A, class IN
            '00 01 51 80', // answer, ttl 86400
            '00 04', // answer, rdlength 4
            'b2 4f a9 83', // answer, rdata 178.79.169.131
        ];
        $bin = $this->convertHexDumpToBinary($this->formatHexDump(implode(' ', $data)));
        $answer = new Answer(['rdata' => $bin, 'name' => 'igor.io']);
        $parser = new \Smorken\Dns\Parsers\Answer;
        $a = $parser->fromBinary($answer);
        $expected = [
            'ttl' => 86400,
            'name' => 'igor.io',
            'type' => Message::TYPE_A,
            'class' => Message::CLASS_IN,
            'data' => '',
            'raw_data' => $this->convertHexDumpToBinary($this->formatHexDump($data[4])),
            'rdata' => '',
        ];
        $this->assertEquals($expected, $a->toArray());
    }

    public function testPTRAnswerFromBinary()
    {
        $data = [
            'c0 0c', // answer, offset pointer to 4.4.8.8...
            '00 0c 00 01', // answer, type PTR, class IN
            '00 01 51 7f', // answer, ttl 86399
            '00 20', // answer, rdlength 32
            '13 67 6f 6f 67 6c 65 2d 70 75 62 6c 69 63 2d 64', // answer, rdata google-public-dns-b.google.com
            '6e 73 2d 62 06 67 6f 6f 67 6c 65 03 63 6f 6d 00',
        ];
        $bin = $this->convertHexDumpToBinary($this->formatHexDump(implode(' ', $data)));
        $answer = new Answer(['rdata' => $bin, 'name' => '8.8.4.4']);
        $parser = new \Smorken\Dns\Parsers\Answer;
        $a = $parser->fromBinary($answer);
        $expected = [
            'ttl' => 86399,
            'name' => '8.8.4.4',
            'type' => Message::TYPE_PTR,
            'class' => Message::CLASS_IN,
            'data' => '',
            'raw_data' => $this->convertHexDumpToBinary($this->formatHexDump($data[4].' '.$data[5])),
            'rdata' => '',
        ];
        $this->assertEquals($expected, $a->toArray());
    }
}
