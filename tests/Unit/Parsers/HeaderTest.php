<?php

namespace Tests\Smorken\Dns\Unit\Parsers;

use PHPUnit\Framework\TestCase;
use Smorken\Dns\Contracts\Models\Message;
use Smorken\Dns\Parsers\Header;
use Tests\Smorken\Dns\Traits\Converts;

class HeaderTest extends TestCase
{
    use Converts;

    public function testQuestionHeaderToBinary()
    {
        $data = '72 62 01 00 00 01 00 00 00 00 00 00';
        $expected = $this->formatHexDump($data);
        $header = new \Smorken\Dns\Models\Header(['id' => 0x7262, 'rd' => true, 'qdcount' => 1]);
        $parser = new Header;
        $bin = $parser->toBinary($header);
        $result = $this->convertBinaryToHexDump($bin);
        $this->assertSame($expected, $result);
    }

    public function testResponseHeaderToBinary()
    {
        $data = '72 62 81 80 00 01 00 01 00 00 00 00';
        $expected = $this->formatHexDump($data);
        $header = new \Smorken\Dns\Models\Header([
            'id' => 0x7262,
            'qr' => true,
            'rd' => true,
            'ra' => true,
            'qdcount' => 1,
            'ancount' => 1,
        ]);
        $parser = new Header;
        $bin = $parser->toBinary($header);
        $result = $this->convertBinaryToHexDump($bin);
        $this->assertSame($expected, $result);
    }

    public function testQuestionHeaderFromBinary()
    {
        $data = '72 62 01 00 00 01 00 00 00 00 00 00';
        $bin = $this->convertHexDumpToBinary($data);
        $header = new \Smorken\Dns\Models\Header(['rdata' => $bin]);
        $parser = new Header;
        $h = $parser->fromBinary($header);
        $this->assertSame(0x7262, $h->id);
        $this->assertFalse($h->qr);
        $this->assertSame(Message::OPCODE_QUERY, $h->opcode);
        $this->assertFalse($h->aa);
        $this->assertFalse($h->tc);
        $this->assertTrue($h->rd);
        $this->assertFalse($h->ra);
        $this->assertSame(Message::RCODE_OK, $h->rcode);
        $this->assertSame(1, $h->qdcount);
        $this->assertSame(0, $h->ancount);
    }

    public function testResponseHeaderFromBinary()
    {
        $data = '72 62 81 80 00 01 00 01 00 00 00 00';
        $bin = $this->convertHexDumpToBinary($data);
        $header = new \Smorken\Dns\Models\Header(['rdata' => $bin]);
        $parser = new Header;
        $h = $parser->fromBinary($header);
        $this->assertSame(0x7262, $h->id);
        $this->assertTrue($h->qr);
        $this->assertSame(Message::OPCODE_QUERY, $h->opcode);
        $this->assertFalse($h->aa);
        $this->assertFalse($h->tc);
        $this->assertTrue($h->rd);
        $this->assertTrue($h->ra);
        $this->assertSame(Message::RCODE_OK, $h->rcode);
        $this->assertSame(1, $h->qdcount);
        $this->assertSame(1, $h->ancount);
    }
}
