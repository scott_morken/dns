<?php

namespace Tests\Smorken\Dns\Unit\Parsers\Questions;

use PHPUnit\Framework\TestCase;
use Smorken\Dns\Contracts\Models\Message;
use Smorken\Dns\Models\Question;
use Tests\Smorken\Dns\Traits\Converts;

class QuestionTest extends TestCase
{
    use Converts;

    public function testAQuestionFromBinary()
    {
        $data = [
            '04 69 67 6f 72 02 69 6f 00', // igor.io
            '00 01 00 01', // type A, class IN
        ];
        $bin = $this->convertHexDumpToBinary($this->formatHexDump(implode(' ', $data)));
        $question = new Question(['rdata' => $bin]);
        $parser = new \Smorken\Dns\Parsers\Question;
        $q = $parser->fromBinary($question);
        $this->assertSame('igor.io', $q->name);
        $this->assertSame(Message::TYPE_A, $q->type);
        $this->assertSame(Message::CLASS_IN, $q->class);
    }

    public function testAQuestionToBinary()
    {
        $data = [
            '04 69 67 6f 72 02 69 6f 00', // igor.io
            '00 01 00 01', // type A, class IN
        ];
        $expected = $this->formatHexDump(implode(' ', $data));
        $question = (new Question)->newQuestion('igor.io', Message::TYPE_A);
        $parser = new \Smorken\Dns\Parsers\Question;
        $bin = $parser->toBinary($question);
        $result = $this->convertBinaryToHexDump($bin);
        $this->assertSame($expected, $result);
    }

    public function testPTRQuestionFromBinary()
    {
        $data = [
            '01 34 01 34 01 38 01 38 07 69 6e', // 4.4.8.8.in-addr.arpa
            '2d 61 64 64 72 04 61 72 70 61 00',
            '00 0c 00 01', // type PTR, class IN
        ];
        $bin = $this->convertHexDumpToBinary($this->formatHexDump(implode(' ', $data)));
        $question = new Question(['rdata' => $bin]);
        $parser = new \Smorken\Dns\Parsers\Question;
        $q = $parser->fromBinary($question);
        $this->assertSame('4.4.8.8.in-addr.arpa', $q->name);
        $this->assertSame(Message::TYPE_PTR, $q->type);
        $this->assertSame(Message::CLASS_IN, $q->class);
    }

    public function testPTRQuestionToBinary()
    {
        $data = [
            '01 34 01 34 01 38 01 38 07 69 6e', // 4.4.8.8.in-addr.arpa
            '2d 61 64 64 72 04 61 72 70 61 00',
            '00 0c 00 01', // type PTR, class IN
        ];
        $expected = $this->formatHexDump(implode(' ', $data));
        $question = (new Question)->newQuestion('4.4.8.8.in-addr.arpa', Message::TYPE_PTR);
        $parser = new \Smorken\Dns\Parsers\Question;
        $bin = $parser->toBinary($question);
        $result = $this->convertBinaryToHexDump($bin);
        $this->assertSame($expected, $result);
    }
}
