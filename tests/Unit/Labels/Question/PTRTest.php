<?php

namespace Tests\Smorken\Dns\Unit\Labels\Question;

use PHPUnit\Framework\TestCase;
use Smorken\Dns\Exception;
use Smorken\Dns\Labels\Question\PTR;

class PTRTest extends TestCase
{
    public function testIpv4()
    {
        $sut = new PTR;
        $to = $sut->to('8.8.4.4');
        $this->assertEquals('4.4.8.8.in-addr.arpa', $to);
        $from = $sut->from($to);
        $this->assertEquals('8.8.4.4', $from);
    }

    public function testIpv6()
    {
        $add = '2001:0db8:85a3:0000:0000:8a2e:0370:7334';
        $sut = new PTR;
        $to = $sut->to($add);
        $expected = '4.3.3.7.0.7.3.0.e.2.a.8.0.0.0.0.0.0.0.0.3.a.5.8.8.b.d.0.1.0.0.2.ip6.arpa';
        $this->assertEquals($expected, $to);
        $this->assertEquals($add, $sut->from($to));
    }

    public function testNotIpIsException()
    {
        $sut = new PTR;
        $this->expectException(Exception::class);
        $sut->to('10.2.303.456');
    }
}
