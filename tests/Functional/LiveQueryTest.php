<?php

namespace Tests\Smorken\Dns\Functional;

use PHPUnit\Framework\TestCase;
use Smorken\Dns\Contracts\Models\Message;
use Smorken\Dns\Factory;
use Smorken\Dns\Handler;
use Smorken\Dns\Pointer;
use Smorken\Dns\Query;
use Smorken\Dns\Transports\Tcp;
use Smorken\Dns\Transports\Udp;

class LiveQueryTest extends TestCase
{
    public function testFactoryCanQueryPTR()
    {
        $message = Factory::query('8.8.4.4', Message::TYPE_PTR);
        $this->assertEquals('dns.google', $message->firstAnswer());
        $this->assertEquals('dns.google', $message->answers[0]->data);
    }

    public function testTcpPTRInvalidServer()
    {
        $ip = '8.8.4.4';
        $sut = $this->getSut([new Tcp('127.0.0.2')]);
        $message = $sut->query($ip, Message::TYPE_PTR);
        $this->assertNull($message->firstAnswer());
        $this->assertEmpty($message->answers);
    }

    public function testTcpPTRPublicDnsServer()
    {
        $ip = '8.8.4.4';
        $sut = $this->getSut([new Tcp('8.8.8.8')]);
        $message = $sut->query($ip, Message::TYPE_PTR);
        $this->assertEquals('dns.google', $message->firstAnswer());
        $this->assertEquals('dns.google', $message->answers[0]->data);
    }

    public function testUdpPTRInvalidServer()
    {
        $ip = '8.8.4.4';
        $sut = $this->getSut([new Udp('127.0.0.2')]);
        $message = $sut->query($ip, Message::TYPE_PTR);
        $this->assertNull($message->firstAnswer());
        $this->assertEmpty($message->answers);
    }

    public function testUdpPTRPublicDnsServer()
    {
        $ip = '8.8.4.4';
        $sut = $this->getSut();
        $message = $sut->query($ip, Message::TYPE_PTR);
        $this->assertEquals('dns.google', $message->firstAnswer());
        $this->assertEquals('dns.google', $message->answers[0]->data);
    }

    /**
     * @return Query
     */
    protected function getSut(array $transports = []): \Smorken\Dns\Contracts\Query
    {
        if (empty($transports)) {
            $transports = [new Udp('8.8.8.8')];
        }
        $handler = new Handler(new Pointer);

        return new Query($handler, $transports);
    }
}
