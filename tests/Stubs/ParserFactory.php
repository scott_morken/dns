<?php

declare(strict_types=1);

namespace Tests\Smorken\Dns\Stubs;

use Smorken\Dns\Contracts\Models\Header;
use Smorken\Dns\Contracts\Models\Model;
use Smorken\Dns\Factories\ModelFactory;

class Ext
{
    protected ModelFactory $factory;

    protected ?Header $header;

    public function __construct()
    {
        $this->factory = new ModelFactory;
    }

    public function getModel(string $modelClass): Model
    {
        return $this->factory->create($modelClass);
    }

    public function initHeader(): void
    {
        $this->header = $this->factory->create(Header::class);
    }
}

$ext = new Ext;
$ext->initHeader();
