## DNS package for PHP/Laravel 6+

### License

This software is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

Much of this library borrowed heavily from [ReactPHP/DNS](https://reactphp.org/dns/)

### Installation

The service provider should automatically register itself under Laravel.
If not, you can manually add `Smorken\Dns\ServiceProvider::class` to the
providers section of `config/app.php`.

Publish the config (under Laravel):
```
$ php artisan vendor:publish --provider="Smorken\Dns\ServiceProvider"
```

### Notes

This is a synchronous dns client (purposefully, for my use case).

I created this for reverse (PTR) lookups.  I added A record lookups as part of testing, but that is
as far as I made it. Further types can be added - feel free to create a pull request if you use it 
and want more types handled.

### Laravel

```php
$query = App::make(\Smorken\Dns\Contracts\Query::class);
$message = $query->query('8.8.4.4', \Smorken\Dns\Contracts\Models\Message::TYPE_PTR);
$hostname = $message->answers[0]->data;
```

### Manual use

#### Factory

```php
// defaults
$message = \Smorken\Dns\Factory::query('8.8.4.4', \Smorken\Dns\Contracts\Models\Message::TYPE_PTR);
$hostname = $message->answers[0]->data;
```

```php
// override query
$transports = [
    new \Smorken\Dns\Transports\Udp('8.8.8.8', 0.5),
    new \Smorken\Dns\Transports\Tcp('8.8.8.8', 0.5),
];
$h = new \Smorken\Dns\Handler(new \Smorken\Dns\Pointer()/*, Model[], Parser[], Label[]*/);
$query = new \Smorken\Dns\Query($h, $transports);
\Smorken\Dns\Factory::$query = $query;
$message = \Smorken\Dns\Factory::query('8.8.4.4', \Smorken\Dns\Contracts\Models\Message::TYPE_PTR);
$hostname = $message->answers[0]->data;
```

#### Query
```php
$transports = [
    new \Smorken\Dns\Transports\Udp('8.8.8.8', 0.5),
    new \Smorken\Dns\Transports\Tcp('8.8.8.8', 0.5),
];
$h = new \Smorken\Dns\Handler(new \Smorken\Dns\Pointer());
$query = new \Smorken\Dns\Query($h, $transports);
$message = $query->query('8.8.4.4', \Smorken\Dns\Contracts\Models\Message::TYPE_PTR);
$hostname = $message->answers[0]->data;
```

#### Handler
```php
$handler = new \Smorken\Dns\Handler(new \Smorken\Dns\Pointer());
$t = new \Smorken\Dns\Transports\Udp('8.8.8.8');
$question_message = $handler->newQuery('8.8.4.4.', \Smorken\Dns\Contracts\Models\Message::TYPE_PTR);
$packet = $handler->toBinary($question_message);
$response = $t->send($packet);
$answer_message = $handler->fromBinary($response);
$hostname = $answer_message->answers[0]->data;
```

